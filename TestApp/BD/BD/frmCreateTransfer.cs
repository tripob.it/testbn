﻿using BD.Domains.Transfers;
using BD.Domains.Contexts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BD.Domains.Accounts;

namespace BD
{
    public partial class frmCreateTransfer : Form
    {
        public frmCreateTransfer()
        {
            InitializeComponent();
            accountService = new AccountsService();
        }
        public bool isCancel = false;
        private int? workStep;
        public TransferService service;
        private AccountsService accountService;
        private List<Account> lsAccountTo;
        private List<Account> lsAccountFrom;

        public void Loading()
        {
            this.progressBar1.Visible = true;
            this.btnSave.Enabled = false;
        }
        public void FinishLoad()
        {
            this.progressBar1.Visible = false;
            this.btnSave.Enabled = true;
        }

        private void frmCreateAccount_Load(object sender, EventArgs e)
        {
            this.workStep = 1;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAmount.Text)) return;
            decimal transferAmount = 0;
            try { transferAmount = Convert.ToDecimal(txtAmount.Text); }
            catch { return; }            
            if (transferAmount <= 0) return;
            var accountFrom =(Guid?)this.ddlFrom.SelectedValue;
            var accountTo = (Guid?)this.ddlTo.SelectedValue;
            if (accountFrom.Value.ToString() == accountTo.Value.ToString())
            {
                MessageBox.Show("Could not transfer to same account");
                return;
            }
            var fromAccount = this.lsAccountFrom.FirstOrDefault(c => c.AccountId == accountFrom.Value);
            if(fromAccount.Deposit.Value < transferAmount)
            {
                MessageBox.Show("Deposit less than transfer amount value");
                return;
            }
            var toAccount = this.lsAccountFrom.FirstOrDefault(c => c.AccountId == accountTo.Value);
            var transfer = new Transfer()
            {
                Amount = transferAmount,
                Type = 2,
                FromAccountId = accountFrom.Value,
                DestinationAccountId = accountTo.Value,
                FromIbanId = fromAccount.IbanId,
                DestionationIbanId = toAccount.IbanId
            };
            this.workStep = 2;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync(transfer);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.workStep)
            {
                case 1: //Load 
                  
                    this.lsAccountTo = accountService.SearchAccounts(new SearchCriteria());
                    this.lsAccountFrom = accountService.SearchAccounts(new SearchCriteria());
                    break;
                case 2: //New Data
                    var transfer = (Transfer)e.Argument;
                    this.service.CreateTransaction(transfer);
                    break;
            }
            e.Result = this;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FinishLoad();
            switch (this.workStep)
            {
                case 1:
                    this.ddlFrom.DisplayMember = "IBanId";
                    this.ddlFrom.ValueMember = "AccountId";
                    this.ddlFrom.DataSource = this.lsAccountFrom;
                    this.ddlTo.DisplayMember = "IBanId";
                    this.ddlTo.ValueMember = "AccountId";
                    this.ddlTo.DataSource = this.lsAccountTo;
                    break;
                case 2:
                    MessageBox.Show("Success");
                    this.isCancel = false;
                    this.Close();
                    break;
              
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
           
        }
    }
}
