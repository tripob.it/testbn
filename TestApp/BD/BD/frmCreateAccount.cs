﻿using BD.Domains.Accounts;
using BD.Domains.Contexts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD
{
    public partial class frmCreateAccount : Form
    {
        public frmCreateAccount()
        {
            InitializeComponent();
            //service = new AccountsService();
        }
        public bool isCancel = false;
        public bool isEdit = false;
        public Guid? accountId;
        private int? workStep;
        public AccountsService service;
        public Account accountData;
        public string ibanId { get; set; }
        public void Loading()
        {
            this.progressBar1.Visible = true;
            this.btnSave.Enabled = false;
        }
        public void FinishLoad()
        {
            this.progressBar1.Visible = false;
            this.btnSave.Enabled = true;
        }
        public string GetIban(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        private void frmCreateAccount_Load(object sender, EventArgs e)
        {
            if (this.isEdit)
            {
                this.workStep = 2;
                this.Loading();
                this.backgroundWorker1.RunWorkerAsync();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIban.Text)) return;
            if (this.accountData == null) this.accountData = new Account();            
            this.accountData.IbanId = this.txtIban.Text;
            this.accountData.AccountName = this.txtName.Text;
            this.workStep = (isEdit == false ? 3 : 4);
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.workStep)
            {
                case 1: //Load Iban
                    var urlIban = @"http://randomiban.com/?country=Netherlands";
                    this.ibanId = this.GetIban(urlIban);
                    break;
                case 2: //Load Data for Edit
                    this.accountData = service.GetAccountById(this.accountId);
                    break;
                case 3: //New Data
                    this.service.InsertAccount(accountData);
                    break;
                case 4: //Edit Data
                    this.service.UpdateAccount(accountData);
                    break;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FinishLoad();
            switch (this.workStep)
            {
                case 1:
                    this.txtIban.Text = this.ibanId;
                    break;
                case 2:
                    this.txtIban.Text = this.accountData.IbanId;
                    this.txtName.Text = this.accountData.AccountName;
                    break;
                case 3:
                    MessageBox.Show("Success");
                    this.isCancel = false;
                    this.Close();
                    break;
                case 4:
                    MessageBox.Show("Success");
                    this.isCancel = false;
                    this.Close();
                    break;
            }
        }
    }
}
