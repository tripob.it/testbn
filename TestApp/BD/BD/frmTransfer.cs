﻿using BD.Domains.Accounts;
using BD.Domains.Contexts;
using BD.Domains.Transfers;
using BD.Domains.Transfers.Vms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD
{
    public partial class frmTransfer : Form
    {
        public frmTransfer()
        {
            InitializeComponent();
            service = new TransferService();
            searchCriteria = new SearchCriteria();
        }        
        public TransferService service;
        private int? working;
        private List<TransferViewModel> dataSource;
        private SearchCriteria searchCriteria;
        private int? selectedIndex;
        private void Loading()
        {
            this.tsbtnDeposit.Enabled = false;
            this.tsbtnTransfer.Enabled = false;            
            this.toolStripProgressBar1.Visible = true;
        }
        private void FinishLoading()
        {
            this.tsbtnDeposit.Enabled = true;
            this.tsbtnTransfer.Enabled = true;
            this.toolStripProgressBar1.Visible = false;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var frm = new frmCreateDeposit();
            frm.service = this.service;
            frm.ShowDialog();
            if (frm.isCancel == true) return;
            this.working = 1;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void tsbtnEdit_Click(object sender, EventArgs e)
        {
            var frm = new frmCreateTransfer();
            frm.service = this.service;
            frm.ShowDialog();
            if (frm.isCancel == true) return;
            this.working = 1;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void frmTransfer_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = false;
            this.working = 1;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.working)
            {
                case 1:
                    this.dataSource = this.service.SearchTransfer(searchCriteria);
                    break;
               
            }
            e.Result = true;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FinishLoading();
            if (e.Result != null)
            {
                if (Convert.ToBoolean(e.Result) && this.working == 1)
                {
                    this.dataGridView1.DataSource = null;
                    this.dataGridView1.DataSource = this.dataSource;
                }
               
            }
        }
    }
}
