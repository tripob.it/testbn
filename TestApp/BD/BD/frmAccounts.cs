﻿using BD.Domains.Accounts;
using BD.Domains.Contexts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD
{
    public partial class frmAccounts : Form
    {
        public frmAccounts()
        {
            InitializeComponent();
            service = new AccountsService();
            searchCriteria = new SearchCriteria();
            
        }
        public bool? isCancel = false;
        public AccountsService service;
        private int? working;
        private List<Account> dataSource;
        private SearchCriteria searchCriteria;
        private int? selectedIndex;
        private void Loading()
        {
            this.tsbtnDelete.Enabled = false;
            this.tsbtnEdit.Enabled = false;
            this.tsbtnAdd.Enabled = false;
            this.toolStripProgressBar1.Visible = true;
        }
        private void FinishLoading()
        {
            this.tsbtnDelete.Enabled = true;
            this.tsbtnEdit.Enabled = true;
            this.tsbtnAdd.Enabled = true;
            this.toolStripProgressBar1.Visible = false;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var frm = new frmCreateAccount();
            frm.service = this.service;
            frm.ShowDialog();
            if (frm.isCancel == true) return;
            this.working = 1;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync(); 
        }

        private void frmAccounts_Load(object sender, EventArgs e)
        {
            this.working = 1;
            this.WindowState= FormWindowState.Maximized;
            this.dataGridView1.AutoGenerateColumns = false;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            switch(this.working)
            {
                case 1:
                    this.dataSource = this.service.SearchAccounts(searchCriteria);
                    break;
                case 2:
                    var accountId = (Guid)e.Argument;
                    this.service.DeleteAccount(accountId);
                    this.dataSource = this.service.SearchAccounts(searchCriteria);
                    break;
            }
            e.Result = true;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FinishLoading();
            if (e.Result != null)
            {
                if(Convert.ToBoolean(e.Result) && this.working == 1)
                {
                    this.dataGridView1.DataSource = null;
                    this.dataGridView1.DataSource = this.dataSource;
                }
                else if (Convert.ToBoolean(e.Result) && this.working ==2)
                {
                    this.dataGridView1.DataSource = null;
                    this.dataGridView1.DataSource = this.dataSource;
                }
            }
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex>-1 && e.ColumnIndex > -1)
            {
                var accountId = (Guid)dataGridView1.Rows[e.RowIndex].Cells["Column4"].Value;
                var frm = new frmCreateAccount();
                frm.service = this.service;
                frm.isEdit = true;
                frm.accountId = accountId;
                frm.ShowDialog();
                if (frm.isCancel == true) return;
                this.working = 1;
                this.Loading();
                this.backgroundWorker1.RunWorkerAsync();
            }
        }

        private void tsbtnDelete_Click(object sender, EventArgs e)
        {
            if(this.dataGridView1.SelectedCells.Count > 0)
            {
                var accountId = (Guid)dataGridView1.Rows[this.selectedIndex.Value].Cells["Column4"].Value;
                if (MessageBox.Show("Delete " + accountId.ToString() + " Confirm?", "Confirm?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    this.working = 2;
                    this.Loading();
                    this.backgroundWorker1.RunWorkerAsync(accountId);
                }                    
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex > -1)
            {
                this.selectedIndex = e.RowIndex;
            }
        }
    }
}
