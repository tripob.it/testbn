﻿using BD.Domains.Accounts;
using BD.Domains.Contexts;
using BD.Domains.Transfers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD
{
    public partial class frmCreateDeposit : Form
    {
        public frmCreateDeposit()
        {
            InitializeComponent();
            service = new TransferService();
            accountsService = new AccountsService();
        }
        public bool isCancel = false;
        public bool isEdit = false;        
        private int? workStep;
        private AccountsService accountsService;
        private List<Account> lsAccountTo;
        public TransferService service;                
        public void Loading()
        {
            this.progressBar1.Visible = true;
            this.btnSave.Enabled = false;
        }
        public void FinishLoad()
        {
            this.progressBar1.Visible = false;
            this.btnSave.Enabled = true;
        }        

        private void frmCreateAccount_Load(object sender, EventArgs e)
        {
            this.workStep = 1;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDeposit.Text)) return;
            decimal depositValue = 0;
            try
            {
                depositValue = Convert.ToDecimal(txtTotal.Text);
            }
            catch { return; }
            if (depositValue == 0)
            {
                MessageBox.Show("Deposit amount require more 0");
                return;
            }

            var transferNew = new Transfer()
            {
                FromAccountId = (Guid?)this.ddlDepositTo.SelectedValue,
                FromIbanId = this.ddlDepositTo.Text.ToString(),
                Type = 1,
                Amount = depositValue,
                fee = Convert.ToDecimal(txtFee.Text)
            };

            this.workStep = 2;
            this.Loading();
            this.backgroundWorker1.RunWorkerAsync(transferNew);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (this.workStep)
            {
                case 1: //Load Account To
                    var accountDatas = accountsService.SearchAccounts(new SearchCriteria());
                    this.lsAccountTo = accountDatas;
                    break;
                case 2: //New Data
                    var argument = (Transfer)e.Argument;
                    this.service.CreateTransaction(argument);

                    break;
            
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.FinishLoad();
            switch (this.workStep)
            {
                case 1:
                    this.ddlDepositTo.DisplayMember = "IBanId";
                    this.ddlDepositTo.ValueMember = "AccountId";
                    this.ddlDepositTo.DataSource = this.lsAccountTo;
                    break;
                case 2:
                    MessageBox.Show("Success");
                    this.isCancel = false;
                    this.Close();
                    break;
               
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtDeposit_TextChanged(object sender, EventArgs e)
        {
            var data = (TextBox)sender;
            if (string.IsNullOrEmpty(data.Text)) return;
            try
            {
                decimal decAmount = Convert.ToDecimal(data.Text);
                if (decAmount == 0) return;
                decimal fee = decAmount * Convert.ToDecimal(0.1);
                fee = Math.Round(fee, 2);
                txtFee.Text = fee.ToString();
                var totalValue = Convert.ToDecimal(decAmount - fee);
                txtTotal.Text = totalValue.ToString("N2");

            }
            catch { }
        }
    }
}
