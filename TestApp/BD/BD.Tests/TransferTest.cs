using BD.Domains.Accounts;
using BD.Domains.Contexts;
using BD.Domains.Transfers;

namespace BD.Tests
{
    public class TransferTest
    {
        public TransferTest()
        {
            serviceAccount = new AccountsService();
            serviceTransfer = new TransferService();
        }
        private AccountsService serviceAccount;
        private TransferService serviceTransfer;
        [Fact]
        public void Test01GetListAccountWillSuccess()
        {
            var searchCriteria = new SearchCriteria();
            var dataSearch = this.serviceTransfer.SearchTransfer(searchCriteria);
            Assert.NotNull(dataSearch);

        }
        [Theory]
        [InlineData("NL46RABO7639905176")]
        [InlineData("NL63INGB3779588560")]
        public void Test02DepositAccountWillBeSuccess(string iBanId)
        {

            var data = serviceAccount.GetAccountByIbanId(iBanId);
            serviceTransfer.CreateTransaction(new Transfer
            {
                Amount = 200,
                fee = Convert.ToDecimal(200 * 0.1),
                FromAccountId = data.AccountId,
                FromIbanId = iBanId,
                Type = 1
            });
            Assert.True(1 == 1);
          
        }
        [Theory]
        [InlineData("NL46RABO7639905176", "NL63INGB3779588560")]
        [InlineData("NL63INGB3779588560", "NL46RABO7639905176")]
        public void Test03TransferAccountWillSuccess(string iBanFrom,string iBanTo)
        {
            var dataFrom = serviceAccount.GetAccountByIbanId(iBanFrom);
            var dataTo = serviceAccount.GetAccountByIbanId(iBanTo);
            var TransferValue = dataFrom.Deposit - 20;
            serviceTransfer.CreateTransaction(new Transfer
            {
                Amount = TransferValue,
                FromAccountId = dataFrom.AccountId,
                FromIbanId = dataFrom.IbanId,
                DestinationAccountId = dataTo.AccountId,
                DestionationIbanId = dataTo.IbanId,
                Type = 2
            });

        }
       

    }
}