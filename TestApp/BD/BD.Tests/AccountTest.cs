using BD.Domains.Accounts;
using BD.Domains.Contexts;

namespace BD.Tests
{
    public class AccountTest
    {
        public AccountTest()
        {
            service = new AccountsService();
        }
        private AccountsService service;
        [Theory]
        [InlineData("NL46RABO7639905176", "Test1")]
        [InlineData("NL63INGB3779588560", "Test23")]
        public void Test01InsertAccountWillBeSuccess(string iBanId, string accountName)
        {
            var newAccount = new Account()
            {
                IbanId = iBanId,
                AccountName = accountName
            };
            service.InsertAccount(newAccount);
            Assert.True(1 == 1);
        }
        [Fact]
        public void Test02GetListAccountWillSuccess()
        {
           var searchCriteria= new SearchCriteria();
           var dataSearch = this.service.SearchAccounts(searchCriteria);
           Assert.NotNull(dataSearch);
         
        }
        [Fact]
        public void Test03GetAccountDetailServiceWillSuccess()
        {
            var searchCriteria = new SearchCriteria();
            var dataSearch = this.service.SearchAccounts(searchCriteria);
            var accountId = dataSearch.Select(c => c.AccountId).FirstOrDefault();
            var dataAccount = this.service.GetAccountById(accountId);
            Assert.NotNull(dataAccount);
        }

    }
}