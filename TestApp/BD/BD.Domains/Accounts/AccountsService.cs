﻿using BD.Domains.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Domains.Accounts
{
    public class AccountsService
    {
        public TestDataContext testDataContext;
        public AccountsService()
        {
            testDataContext = new TestDataContext();
        }
       
        public List<Account> SearchAccounts(SearchCriteria searchCriteria)
        {
            IQueryable<Account> result;
            if (!string.IsNullOrEmpty(searchCriteria.Keyword))
            {
                result = this.testDataContext.Accounts.Where(c => c.IbanId.Contains(searchCriteria.Keyword) || c.AccountName.Contains(searchCriteria.Keyword))
                    .Take(searchCriteria.PageSize.Value)
                    .Skip((searchCriteria.PageActive.Value * searchCriteria.PageSize.Value));
            }
            else
            {
                result = this.testDataContext.Accounts
                 .Take(searchCriteria.PageSize.Value)
                 .Skip(((searchCriteria.PageActive.Value-1) * searchCriteria.PageSize.Value));
            }
            return result.ToList();

        }
        public Account GetAccountById(Guid? accountId)
        {
            var result = this.testDataContext.Accounts.FirstOrDefault(c => c.AccountId == accountId.Value);
            return result;
        }
        public Account GetAccountByIbanId(string? ibanId)
        {
            var result = this.testDataContext.Accounts.FirstOrDefault(c => c.IbanId == ibanId);
            return result;
        }
        public void InsertAccount(Account account)
        {
            if (account == null) return;
            try
            {
                var findDuplicate = testDataContext.Accounts.Where(c => c.IbanId == account.IbanId).ToList();
                if (findDuplicate.Any())
                {
                    throw new Exception("Duplicate IBan");
                }                
                account.AccountId = Guid.NewGuid(); 
                account.CreateDate = DateTime.Now;
                testDataContext.Accounts.Add(account);
                this.testDataContext.SaveChanges();
            }
            catch
            {
                throw; 
            }
        }
        public void UpdateAccount(Account account)
        {
            if (account == null) return;
            try
            {

                var accFind = testDataContext.Accounts.Where(c => c.AccountId == account.AccountId).FirstOrDefault();
                if (accFind != null)
                {
                    accFind.AccountName = account.AccountName;
                    accFind.UpdateDate = DateTime.Now;
                    this.testDataContext.Entry(accFind).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
                this.testDataContext.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        public void DeleteAccount(Guid? accountId)
        {
            if (accountId == null) return;
            try
            {

                var accFind = testDataContext.Accounts.Where(c => c.AccountId == accountId).FirstOrDefault();
                if (accFind != null)
                {                   
                    this.testDataContext.Entry(accFind).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                }
                this.testDataContext.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

    }
    public class SearchCriteria
    {
        public int? PageSize { get; set; } = 100;
        public int? PageActive { get; set; } = 1;
        public string? Keyword { get; set; }
    }
}
