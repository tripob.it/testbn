﻿using System;
using System.Collections.Generic;

namespace BD.Domains.Contexts
{
    public partial class Account
    {
        public Guid AccountId { get; set; }
        public string? IbanId { get; set; }
        public string? AccountName { get; set; }
        public decimal? Deposit { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
    }
}
