﻿using System;
using System.Collections.Generic;

namespace BD.Domains.Contexts
{
    public partial class Transfer
    {
        public long TransferId { get; set; }
        public Guid? FromAccountId { get; set; }
        public string? FromIbanId { get; set; }
        public Guid? DestinationAccountId { get; set; }
        public string? DestionationIbanId { get; set; }
        public int? Type { get; set; }
        public decimal? Amount { get; set; }
        public decimal? fee { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
