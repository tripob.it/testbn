﻿using BD.Domains.Accounts;
using BD.Domains.Contexts;
using BD.Domains.Transfers.Vms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD.Domains.Transfers
{
    public class TransferService
    {
        public TestDataContext testDataContext;
        public TransferService()
        {
            testDataContext = new TestDataContext();
        }
       
        public List<TransferViewModel> SearchTransfer(SearchCriteria searchCriteria)
        {
            IQueryable<Transfer> result;
            result = this.testDataContext.Transfers
                    .OrderByDescending(c => c.CreatedDate)
                  .Take(searchCriteria.PageSize.Value)
                  .Skip(((searchCriteria.PageActive.Value - 1) * searchCriteria.PageSize.Value));
            var data = result.ToList();
            if (data != null && data.Count > 0)
            {
                var resultData = (from c in data
                                  select new TransferViewModel
                                  {
                                      TransferId = c.TransferId,
                                      FromAccountId = c.FromAccountId,
                                      FromIbanId = c.FromIbanId,
                                      DestionationIbanId = c.DestionationIbanId,
                                      DestinationAccountId = c.DestinationAccountId,
                                      Amount = c.Amount,
                                      fee = c.fee,
                                      Type = c.Type,
                                      TypeName = (c.Type.HasValue && c.Type == 1 ? "Deposit" : "Transfer"),
                                      CreatedDate= c.CreatedDate
                                  }).ToList();
                return resultData.ToList();
            }
            else
            {
                return new List<TransferViewModel>();
            }
        }
        public Transfer GetTransferDetail(int? transferId)
        {
            var result = this.testDataContext.Transfers.FirstOrDefault(c => c.TransferId == transferId.Value);            
            return result;
        }
        public void CreateTransaction(Transfer transfer)
        {
            if (transfer == null) return;
            try
            {
                if (transfer.Type == 1)
                {
                    var fromId = this.testDataContext.Accounts.Where(c => c.AccountId == transfer.FromAccountId).FirstOrDefault();
                    if (fromId != null)
                    {
                        if (fromId.Deposit.HasValue == false) fromId.Deposit = transfer.Amount;
                        else
                        {
                            fromId.Deposit = fromId.Deposit.Value + transfer.Amount.Value;
                        }
                        testDataContext.Entry(fromId).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    }
                }
                else if (transfer.Type == 2)
                {
                    var fromId = this.testDataContext.Accounts.Where(c => c.AccountId == transfer.FromAccountId).FirstOrDefault();
                    var toId = this.testDataContext.Accounts.Where(c => c.AccountId == transfer.DestinationAccountId).FirstOrDefault();
                    if (fromId != null)
                    {
                       
                        fromId.Deposit = fromId.Deposit.Value - transfer.Amount.Value;                        
                        testDataContext.Entry(fromId).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    }
                    if (toId != null)
                    {
                        toId.Deposit = toId.Deposit.Value+ transfer.Amount.Value;
                        testDataContext.Entry(toId).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    }
                }
                transfer.CreatedDate= DateTime.Now; 
                testDataContext.Transfers.Add(transfer);
                this.testDataContext.SaveChanges();

            }
            catch
            {
                throw; 
            }
        }        

    }  
}
